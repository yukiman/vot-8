
<center><h1>VOT-8 Build instructions</h1></center>

___

Welcome to the VOT-8 build instructions. Here we will have details instructions on how to build (solder) a VOT-8 Voltmeter Kit

<img src="./imgs/mess.jpg">


___

<center><h1>First Step is to identify all the parts </h1></center>

___

<center><h1>Resistors (110k, 10k)</h1></center>

<img src="./imgs/resistors.jpg">



___

<center><h1>Resistors (220)</h1></center>

<img src="./imgs/220-resistors.jpg">



___

<center><h1>Alligator Clips</h1></center>

<img src="./imgs/Alligator Clips.jpg">


___

<center><h1>Board Back</h1></center>

<img src="./imgs/board-2.jpg">

___

<center><h1>Board Front</h1></center>

<img src="./imgs/board.jpg">

___

<center><h1>Button Switch</h1></center>

<img src="./imgs/Button-Switch.jpg">

___

<center><h1>Display</h1></center>

<img src="./imgs/Display.jpg">


___

<center><h1>IC</h1></center>

<img src="./imgs/IC.jpg">


___

<center><h1>Socket</h1></center>

<img src="./imgs/Socket.jpg">


___

<center><h1>Standoffs</h1></center>

<img src="./imgs/Standoffs.jpg">


___

<center><h1>Wire</h1></center>

<img src="./imgs/Test-Wire.jpg">



___
<center><h1>connector</h1></center>

<img src="./imgs/Two-wire-connector.jpg">



---
<center><h1>Voltage Regulator</h1></center>

<img src="./imgs/Voltage-regulator.jpg">


---
<center><h1>capacitors</h1></center>

<img src="./imgs/capacitors.jpg">



---
<center><h1>diodes</h1></center>

<img src="./imgs/diodes.jpg">


---
<center><h1>potentiometer</h1></center>

<img src="./imgs/potentiometer.jpg">

___

<center><h1>Thats a lot of parts to build a Volt Meter (not really)</h1></center>

___


<center><h1>Lets Build it </h1></center>

___


Step 1. 

Solder each of the 220 Resistors on the board (R3 - R10)

<img src="./imgs/S1 Soder 220.jpg" width="800" height="600">

___

Step 1. Done

<img src="./imgs/S1 Done.jpg" width="800" height="600">

___

Step 2.

Solder each of the small capacitors (C1, C3)

<img src="./imgs/S2 Soder Cap small.jpg" >

___

Step 3.

Solder Large capacitors (C2, NOTE polarity matters, Long Line is +)
Leave some extra room so the Cap can lay down to save height

<img src="./imgs/S2 Soder Cap small.jpg">

___

Step 4.

Solder Voltage Regulator (U2, Care full note orentation )
-How to find out what pin goes where ?

<img src="./imgs/S4 Soder VR.jpg">

___

Step 5. 

Solder each of the Other Resistors on the board (R1, R2)
But which is which 
Hard way (https://www.wikihow.com/Read-Axial-Lead-Resistors)
Easy Way use voltmeter to test resistor value

___

Step 6.

Solder diodes on the board (D1)
Notice the lines on each device, denoting the Cathode side (-)
<img src="./imgs/diodes_solder.jpg">

___


Step 7.

Solder switch on the board (S1)

<img src="./imgs/Done.jpg">

___

Step 8.

Solder potentiometer on the board (S1, Note orentation )

<img src="./imgs/Done.jpg">


___

Step 9.

Solder socket on the board (making sure to alignment notch)

<img src="./imgs/Socket_board.jpg">

___

Step 10.

Solder Screen (note aligment of the dots :( )

<img src="./imgs/Done.jpg">

___

Step 11.

Solder Screen (note aligment of the dots)

<img src="./imgs/Done.jpg">

___

Step 12.

Solder Two Wire connector (J1, note location)

<img src="./imgs/Done.jpg">

___

Step 13.

Plug in IC (note alignment notch, slow steady preasure making sure each pin is in correct location)

<img src="./imgs/IC.jpg">

___

Step 14.

screw in standofs 

<img src="./imgs/standoffs_done.jpg"  width="800" height="600">

___

Step 15.

Alligator Clips need to be soldered to test wires based on colors 
seperate clip cover

<img src="./imgs/clips1.jpg"  width="500" height="400">

___

Step 15b.
strip wire and insert boot 

<img src="./imgs/clips2.jpg">


___

Step 15c.
feed striped wire though hole and around side for secure connection 

<img src="./imgs/clips3.jpg"  width="800" height="600">

___

Step 15d.
Solder wires(red line) to clip and fold down 2 tabs  for secure mechanical connection

<img src="./imgs/clips4.jpg"  width="800" height="600">


___

Step 15e.
move boot back over entire clip to cover up the mess 

<img src="./imgs/done.jpg" >



___


https://gitlab.com/yukiman/vot-8






